<?php

require_once 'config/functions.php';

session_start();

if ($_POST['scope']) {
    if ($_POST['scope'] == 'like' && $_POST['id_photo']) {
        if (ft_liked($_SESSION['user_id'], $_POST['id_photo'])) {
            ft_unlike($_SESSION['user_id'], $_POST['id_photo']);
            echo "unlike";
        }else {
            ft_like($_SESSION['user_id'], $_POST['id_photo']);
            echo "like";
        }
    }elseif ($_POST['scope'] == 'comment' && $_POST['id_photo'] && $_POST['content']) {
        echo ft_add_comment($_SESSION['user_id'], $_POST['id_photo'], $_POST['content']);
    }
}
?>