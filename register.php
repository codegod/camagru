<?php

$PageTitle = "Camagru | Register";

require_once "header.php";
require_once "config/functions.php";

session_start();

if ($_POST['login'] && $_POST['password'])
{
    $pass = hash("whirlpool", $_POST['password']);
    $res = ft_get_query("SELECT * FROM `users` WHERE login like '".$_POST['login']."';");
    if ($res)
        $err = "This login allready exists!";
    $res = ft_get_query("SELECT * FROM `users` WHERE email like '".$_POST['email']."';");
    if ($res)
        $err = "This email allready exists!";
    if ($_POST['password'] != $_POST['repassword'])
        $err = "Password and retyped password not match!";
    elseif (strlen($_POST['password']) < 8)
        $err = "Password too short!";
    elseif (!preg_match("#[0-9]+#", $_POST['password']))
        $err = "Password must include at least one number!";
    elseif (!preg_match("#[a-zA-Z]+#", $_POST['password']))
        $err = "Password must include at least one letter!";
    elseif (strlen($_POST['number']) != 9)
        $err = "Enter an valid number!";
        
    $ph = 1;
    
    if (!isset($err))
    {
        if (!empty($_FILES['image']['name']))
        {
            $imageFileType = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);  
            $target_dir = "/assets/images/" ."image_". hash("crc32", basename($_FILES["image"]["name"]) . time() . rand(1, 100)) . "." . $imageFileType;
            $target_file = $_SERVER['DOCUMENT_ROOT'] . $target_dir;
            $uploadOk = 1;

            if (file_exists($target_file)) {
                echo "Sorry, file already exists.";
                $uploadOk = 0;
            }

            if ($_FILES["image"]["size"] > 500000) {
                echo "Sorry, your file is too large.";
                $uploadOk = 0;
            }

            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }

            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";

            } else {
                if (!move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
                    echo "Sorry, there was an error uploading your file.";
                }
            }
            ft_execute_query("INSERT INTO `photos` (`id`, `address`) VALUES (NULL, '".$target_dir."');");
            $res = ft_get_query("SELECT * FROM  `photos` WHERE address LIKE  '".$target_dir."'");
            $ph = $res['id'];
        }
        
       print_r(ft_execute_query("INSERT INTO `users` (`surname`, `name`, `email`, `number`, `login`, `password`, `id_photo`,
        `birth_date`, `signup_date`) VALUES ('".$_POST['surname']."', '".$_POST['name']."', '".$_POST['email']."', '".$_POST['number']."', '".$_POST['login']."',
        '".hash("whirlpool", $_POST['password'])."', '".$ph."', '".$_POST['birth_date']."', '".date("Y-m-d")."');"));
        
        $user = ft_get_query("SELECT * FROM  `users` WHERE login LIKE '".$_POST['login']."';");
        
        ft_execute_query("INSERT INTO `ctrler_priv` (`id_user`, `id_priv`) VALUES ('".$user['id']."', '2');");
        
        send_mail_confirm($user);
        
        header("Location: /");
    }
}

if (!isset($_SESSION['logged_in']) && !isset($_SESSION['user_id']))
{
    ?>
    <h1 class="logologin reglogin">
        <a href="/">Camagru</a>
    </h1>
    <form action="/register.php" method="POST" enctype="multipart/form-data">
        <div id="form" class="reglogin">
                    <center>
                        <a href="/login.php">&lt; Back to login</a><h3>Register</h3>
                    </center>
                    <label for="name">Name</label>
                    <input type="text" id="name" name="name" size="15" maxlength="15" value="<?php echo $_POST["name"]; ?>" required/>
                    <label for="surname">Surname</label>
                    <input type="text" id="surname" name="surname" size="15" maxlength="15" value="<?php echo $_POST["surname"]; ?>" required/>
                    <label for="email">Email</label>
                    <input type="email" id="email" name="email" size="30" maxlength="30" value="<?php echo $_POST["email"]; ?>" required/>
                    <label for="number">Number of telephone</label>
                    <input type="text" id="number" name="number" onkeypress='validate(event)' value="<?php echo $_POST["number"]; ?>" required/>
                    <label for="login">Login</label>
                    <input type="text" id="login" name="login" size="8" maxlength="8" value="<?php echo $_POST["login"]; ?>" required/>
                    <label for="password">Password</label>
                    <input type="password" id="password" name="password" size="15" maxlength="15"  required/>
                    <label for="password">Retype Password</label>
                    <input type="password" id="repassword" name="repassword" size="15" maxlength="15" required/>
                    <label for="birth_date">Birthdate</label>
                    <input type="date" id="birth_date" name="birth_date" value="<?php echo $_POST["birth_date"]; ?>" required/>
                    <label for="image">Profile image</label>
                    <input type="file" id="image" name="image"/>
                    <?php
                    if (isset($err))
                    {
                    ?>
                    <p style="color:red;">Error: <?php echo $err; ?></p>
                    <?php
                    }
                    ?>
                    <input class="subbutton" id="submit" type="submit" value="Submit"/>
            </div>
    </form>
    <script type="text/javascript">
        function validate(evt) {
            var inp = document.getElementById("number");
            
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            if (inp.value.length <= 8) {
                key = String.fromCharCode( key );
                var regex = /[0-9]|\./;
                if( !regex.test(key) ) {
                    theEvent.returnValue = false;
                    if(theEvent.preventDefault)
                        theEvent.preventDefault();
                }
            } else {
                theEvent.returnValue = false;
                if(theEvent.preventDefault)
                    theEvent.preventDefault();
            }
        }
    </script>
    <?php
}
else
{
    header("Location: /");
}

?>