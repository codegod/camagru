<?php
$PageTitle = "Camagru | Wall";

require_once "header.php";
require_once "menu.php";

session_start();

if ($_GET['action'] == "editphotos") {


 $sql = "select photos.id, photos.address, users.name, users.surname, 
        count(distinct likes.id) as likes, count(distinct comments.id) as comments from ctrler_photo 
        inner join users on users.id=ctrler_photo.id_user 
        inner join photos on ctrler_photo.id_photo=photos.id 
        left join likes on likes.id_photo=photos.id 
        left join comments on comments.id_photo=photos.id where ctrler_photo.id_user = ".$_SESSION['user_id']." group by photos.id;";
        
        $photos = ft_get_all_queries($sql);

        ?>
        <div id="content">
            <p>Click on photo what you want to delete:</p>
<?php
if ($photos)
    foreach ($photos as $photo) {
    ?>
                   
                    
            <div class="product" id="photo<?php echo $photo['id']; ?>">
                    <img style="cursor:pointer;"  onclick="delete_photo(<?php echo $photo['id']; ?>)" src="<?php echo $photo['address']; ?>" />
                    <address>
                        <span>Comments: <?php echo $photo['comments'];?> Likes: <?php echo $photo['likes']; ?></span>
                    </address>
                
            </div>
    <?php }
    else {
    ?>      
      <center><h3>No photos added by you :((</h3></center>
    <?php } ?>
     </div>
    <script type="text/javascript">
        function delete_photo(id_photo) {
            
            if (confirm("Are you sure what you want to delete this photo?")){
             //XHRHttpRequest---------
      
              var xhr = new XMLHttpRequest();
              xhr.open('POST', '/processor.php');
              xhr.onreadystatechange = function(){
                  if(xhr.readyState != 4 || xhr.status != 200)
                    return;
                  var test = document.getElementById('photo'+id_photo);
                    test.remove();
              }
              xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
              var res = 'scope=delete&id_p=' + id_photo;
              xhr.send(res);
              
              //end
              
            }
            
        }
    </script>

<?php
} if ($_GET['action'] == "edituser") {
    
        if ($_POST['name'] && $_POST['surname'] && $_POST['username'] && $_POST['birth_date']) {
            
            $name = $_POST['name'];
            $surname = $_POST['surname'];
            $username = $_POST['username'];
            $date = $_POST['birth_date'];
            $sql = "update users set name='$name', surname='$surname', login='$username', birth_date='$date' where id=".$_SESSION['user_id'].";";

            ft_execute_query($sql);
            
            if (!empty($_FILES['image']['name']))
            {
                $imageFileType = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);  
                $target_dir = "/assets/images/" ."image_". hash("crc32", basename($_FILES["image"]["name"]) . time() . rand(1, 100)) . "." . $imageFileType;
                $target_file = $_SERVER['DOCUMENT_ROOT'] . $target_dir;
                $uploadOk = 1;
  
                if (file_exists($target_file)) {
                    echo "Sorry, file already exists.";
                    $uploadOk = 0;
                }
    
                if ($_FILES["image"]["size"] > 500000) {
                    echo "Sorry, your file is too large.";
                    $uploadOk = 0;
                }
    
                if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                && $imageFileType != "gif" ) {
                    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                    $uploadOk = 0;
                }
    
                if ($uploadOk == 0) {
                    echo "Sorry, your file was not uploaded.";
    
                } else {
                    if (!move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
                        echo "Sorry, there was an error uploading your file.";
                    }
                }
                $user = ft_get_query("select login, name, surname, address, id_photo, birth_date from users join photos on photos.id=id_photo where users.id=".$_SESSION['user_id'].";");
                
                ft_execute_query("INSERT INTO `photos` (`id`, `address`) VALUES (NULL, '".$target_dir."');");
                
                $res = ft_get_query("SELECT * FROM  `photos` WHERE address LIKE  '".$target_dir."'");
                
                $ph = $res['id'];
                if ($user['id_photo'] != 1) {
                    ft_execute_query("delete from photos where id=".$user['id_photo'].";");
                    unlink($_SERVER['DOCUMENT_ROOT'].$user['address']);
                    ft_execute_query("update users set id_photo=$ph where id=".$_SESSION['user_id'].";");
                } else {
                    ft_execute_query("update users set id_photo=$ph where id=".$_SESSION['user_id'].";");
                }
            }
        }

    
    $sql = "select login, name, surname, address, birth_date from users join photos on photos.id=id_photo where users.id=".$_SESSION['user_id'].";";
    $user = ft_get_query($sql);
    
    ?>
    <div id="constent">
        <img id="userphoto" style="cursor:pointer;" title="Choose another photo" src="<?php echo $user['address']; ?>" width="200" height="150"/>
        <form id="formuser" action="user.php?action=edituser" enctype="multipart/form-data" method="POST">
        <label for="username">Username:</label>
        <input type="text" name="username" id="username" value="<?php echo $user['login']; ?>"/><br>
        <label for="name">Name:</label>
        <input type="text" name="name" id="name" value="<?php echo $user['name']; ?>"/><br>
        <label for="surname">Surname:</label>
        <input type="text" name="surname" id="surname" value="<?php echo $user['surname']; ?>"/><br>
        <label for="surname">Birth-date:</label>
        <input type="date" name="birth_date" value="<?php echo $user['birth_date']; ?>"/><br>
        <input type="file" accept="image/*" id="photouser" name="image" style="display:none;"/>
        <input type="submit" class="subbutton" name="submit" value="Update"/><br>
        </form>
    <script type="text/javascript">
        var photo = document.getElementById("userphoto");
        var inputt = document.getElementById("photouser");
        photo.addEventListener("click", function(){
           inputt.click(); 
        });
    </script>
    <?php
}
require_once "footer.php";
?>