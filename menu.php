<?php

require_once "config/functions.php";

session_start();
if (!isset($_SESSION['logged_in']) && !isset($_SESSION['user_id']))
    header("Location: /login.php");

ft_not_expired_session();

  $user = ft_is_logged_in();


?>
 <nav id="primary_nav_wrap">
            <ul>
                <li><a style="font-family: 'Clicker Script', cursive; font-size:42px; float:left; clear:both;" href="/">Camagru</a></li>
            </ul>
            <ul style="float:right;">
              <?php
              if (!isset($user))
              {
              ?>
                <li><a href="/login.php">Login</a></li>
                <li><a href="/register.php">Register</a></li>
                <?php
              }
              else
              {
                ?>
                <li><a href="#" onclick="event.preventDefault()"><?php echo $user['name']. " ".$user['surname']; ?></a>
                <ul>
                  </li>
                  <?php if (ft_is_admin())
                  {
                  ?>
                  <li><a href="/admin.php">Admin Panel</a></li>
                  <?php
                  }
                  ?>
                  <li><a href="/user.php?action=edituser">Settings</a></li>
                  <li><a href="/user.php?action=editphotos">Delete photos</a></li>
                  <li><a href="/logout.php">Logout</a></li>
                </ul>
                </li>
                <?php
              }
                ?>
            </ul><br><br>
<ul>
  <li ><a href="/">Home</a></li>
  
  <li><a href="#" onclick="event.preventDefault()">Wall</a>
    <ul>
      <li><a href="/wall.php?show=wall">All photos</a></li>
      <li><a href="/wall.php?show=photos">Photos</a></li>
      <li><a href="/wall.php?show=users">Users</a></li>
    </ul>
  </li>
  
  <li><a href="/information.php">About Us</a></li>
</ul>
</nav>