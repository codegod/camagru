<?php
$PageTitle = "Camagru | Home Page";

$includes = array('main');
require_once "header.php";
require_once "menu.php";
require_once "config/database.php";
$effects = ft_get_all_queries("select photos.id as id, ctrler_effects.name as name, photos.address as address from photos inner join ctrler_effects on ctrler_effects.id_photo=photos.id;");
$uid = $_SESSION['user_id'];
$photos = ft_get_all_queries('select photos.id as id, photos.address as address from ctrler_photo inner join photos on photos.id=ctrler_photo.id_photo where id_user='.$uid.' order by id desc limit 5;');

?>
<div style="margin:10px;">
<form>
  <div class="container">
    <div class="left-col">
      <p>Choose an effect from here:</p>
      <div>
        <?php if ($effects) foreach ($effects as $key => $effect) { ?>
        <div style="width:200px; display:inline;">
          <input class='effectsin'  id='image<?php echo $effect['id']; ?>'  type="radio" name="effect" value='<?php echo $effect['id']; ?>'/>
        <label for='image<?php echo $effect['id']; ?>'>
          <img id="imagesel" src="<?php echo $effect['address']; ?>" />
        </label>
        </div>
        <?php } ?>
      </div>
      <div class="wrapper">
        <video id="video"></video>
        <br>
        <button id="startbutton" class='button' disabled>Choose an effect</button>
        <button id="startbutton1" class='button button-small first-btn' style="display:none;">Take photo</button>
        <button id="startbutton2" class='button button-small' style="display:none;">Upload photo</button>
        <input type="file" accept="image/png" id="uploadinput" name="uploadinput" style="display:none;"/>
      </div>
    </div>
    <p>Photos added by you:</p>
    <div class="right-col">
      <div class="wrapper" id='pphotos'>
        <?php if ($photos){ foreach($photos as $photo) { 
          $last_id = $photo['id'];  ?>
        <img src="<?php echo $photo['address']; ?>"   width='320' height='261' id="photo" alt="photo">
        
        <?php }
        ?>
        
        <script type="text/javascript">
           var last_id = <?php echo $last_id; ?>;
         </script>
         <div style="width:100%;" id='wrload'>
             <button id="loadphotos" class='button button-small'>Load more</button>
          </div>
        
        <?php
        }else{ ?>

          <img id="default" src="/assets/images/default.png" width='320' height='261' id="photo" alt="photo">

         <?php } ?>
         
      </div>
          
    </div>
  </div>
</form>
<canvas id="canvas" style='display:none;'></canvas>
<script type="text/javascript">
    (function() {
      
    File.prototype.convertToBase64 = function(callback){
      var reader = new FileReader();
      reader.onload = function(e) {
        callback(e.target.result)
      };
      reader.onerror = function(e) {
        callback(null, e);
      };        
      reader.readAsDataURL(this);
    };

  var streaming = false,
      video        = document.querySelector('#video'),
      cover        = document.querySelector('#cover'),
      canvas       = document.querySelector('#canvas'),
      photo        = document.querySelector('#photo'),
      startbutton  = document.querySelector('#startbutton'),
      startbutton1  = document.querySelector('#startbutton1'),
      startbutton2  = document.querySelector('#startbutton2'),
      uploadinput  = document.querySelector('#uploadinput'),
      loadphotos  = document.querySelector('#loadphotos'),
      width = 500,
      height = 0,
      effects = document.getElementsByName('effect');
      for(var i=0;i<effects.length;i++)
        effects[i].addEventListener('click',function(){
          startbutton.setAttribute('style', 'display:none;');
          startbutton1.setAttribute('style', 'display:block;background:#2ecc71;');
          startbutton2.setAttribute('style', 'display:block;background:#2ecc71;');
        });

  navigator.getMedia = ( navigator.getUserMedia ||
                         navigator.webkitGetUserMedia ||
                         navigator.mozGetUserMedia ||
                         navigator.msGetUserMedia);

  navigator.getMedia(
    {
      video: true,
      audio: false
    },
    function(stream) {
      if (navigator.mozGetUserMedia) {
        video.mozSrcObject = stream;
      } else {
        var vendorURL = window.URL || window.webkitURL;
        video.src = vendorURL.createObjectURL(stream);
      }
      video.play();
    },
    function(err) {
      console.log("An error occured! " + err);
    }
  );
  if (loadphotos)  loadphotos.addEventListener("click", function(ev) {
      ev.preventDefault();
      //XHRHttpRequest---------
      
      var xhr = new XMLHttpRequest();
      xhr.open('POST', '/load_more.php');
      xhr.onreadystatechange = function(){
          if(xhr.readyState != 4 || xhr.status != 200)
            return;
          var parent = document.getElementById('pphotos');

          var butt = document.getElementById('wrload');

          var loadedph = document.createElement("div");
          loadedph.innerHTML = xhr.responseText;
          
          eval(loadedph.childNodes.item(loadedph.childNodes.length - 1).innerText);
          
          for (var i = 0; i < loadedph.childNodes.length; i++) {
            parent.insertBefore(loadedph.childNodes.item(i), butt);
          }


      }
      xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      var res = 'last_id=' + last_id;
    
      xhr.send(res);
      
      //end
  });

  video.addEventListener('canplay', function(ev){
    if (!streaming) {
      height = video.videoHeight / (video.videoWidth/width);
      video.setAttribute('width', width);
      video.setAttribute('height', height);
      canvas.setAttribute('width', width);
      canvas.setAttribute('height', height);
      streaming = true;
    }
  }, false);

  function takepicture() {
    canvas.width = width;
    canvas.height = height;
    canvas.getContext('2d').drawImage(video, 0, 0, width, height);
    var data = canvas.toDataURL('image/png');
    var chh, image;
    
    for(var i=0;i<effects.length;i++)
        if(effects[i].checked){
          image = effects[i].value;
          chh = effects[i];
        }
          
      //XHRHttpRequest---------
      
      var xhr = new XMLHttpRequest();
      xhr.open('POST', '/processor.php');
      xhr.onreadystatechange = function(){
          if(xhr.readyState != 4 || xhr.status != 200)
            return;
          var test = document.getElementById('default');
          if (test != null)
            test.remove();
          var nimg = document.createElement('img');
          nimg.src = xhr.responseText;
          nimg.width = 320; 
          nimg.height = 261;
          document.getElementById('pphotos').append(nimg);

      }
      xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      var res = 'image=' + data + '&id_effect=' + image;
      chh.checked = false;
      //reset
      
      startbutton.setAttribute('style', 'display:block;');
      startbutton1.setAttribute('style', 'display:none;');
      startbutton2.setAttribute('style', 'display:none;');
      
      xhr.send(res);
      
      //end

  }

  startbutton1.addEventListener('click', function(ev){
      
      takepicture();
      
      ev.preventDefault();
      
  }, false);
  
  startbutton2.addEventListener('click', function(ev){
      
      ev.preventDefault();
      uploadinput.click();
      
  }, false);
  
  uploadinput.addEventListener("change", function (){
    var selectedFile = this.files[0];
    selectedFile.convertToBase64(function(base64){
    var chh, image;
    
    for(var i=0;i<effects.length;i++)
        if(effects[i].checked){
          image = effects[i].value;
          chh = effects[i];
        }
      
    //XHRHttpRequest---------
    
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/processor.php');
    xhr.onreadystatechange = function(){
        if(xhr.readyState != 4 || xhr.status != 200)
          return;
        var test = document.getElementById('default');
        if (test != null)
          test.remove();
        var nimg = document.createElement('img');
        nimg.src = xhr.responseText;
        nimg.width = 320; 
        nimg.height = 261;
        document.getElementById('pphotos').append(nimg);
    
    }
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var res = 'image=' + base64 + '&id_effect=' + image;
    chh.checked = false;
    //reset
    
    startbutton.setAttribute('style', 'display:block;');
    startbutton1.setAttribute('style', 'display:none;');
    startbutton2.setAttribute('style', 'display:none;');
    
    xhr.send(res);
    
    //end
      
    startbutton2.value = "";
    }); 
  });

})();
</script>
</div>
<?php
require_once "footer.php";
?>