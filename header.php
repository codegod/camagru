<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php echo $PageTitle ? $PageTitle : "Camagru"; ?></title>
        <link rel="stylesheet" type="text/css" href="/assets/css/style.css"> 
        <link rel="stylesheet" href="/assets/css/menu.css" type="text/css" /> 
        <link href="https://fonts.googleapis.com/css?family=Clicker+Script" rel="stylesheet">
        <link rel="icon" type="image/png" href="/assets/images/favicon.png">
<?php 
    if (isset($includes)) {
        foreach ($includes as $include) {
            echo "\t<link rel='stylesheet' href='/assets/css/" . $include . ".css' type='text/css' />\n";
        }
    }
    if (isset($extraIncludes)) {
        foreach ($extraIncludes as $include) {
            echo "\t<link rel='stylesheet' href='" . $include . "' type='text/css' />\n";
        }
    }
?>
    </head>
    <body>