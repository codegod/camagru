<?php
$PageTitle = "Camagru | Add effect";
require_once "header.php";
require_once "config/functions.php";
require_once "menu_admin.php";

if (ft_is_admin())
{
    
    if ($_POST['effect_name'] && $_POST['submit'])
    {
            
        if ($_FILES["image"]["error"] == UPLOAD_ERR_OK)
        {
                
            $imageFileType = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);  
            
            $target_dir = "/assets/images/" ."image_". hash("crc32", basename($_FILES["image"]["name"][$key]) . time() . rand(1, 100)) . "." . $imageFileType;
            $target_file = $_SERVER['DOCUMENT_ROOT'] . $target_dir;
            $uploadOk = 1;

            if (file_exists($target_file)) {
                echo "Sorry, file already exists.";
                $uploadOk = 0;
            }


            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }

            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";

            } else {
                if (!move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
                    echo "Sorry, there was an error uploading your file.";
                }else{
                    $addedd = 1;
                    ft_execute_query("INSERT INTO `photos` (`id`, `address`) VALUES (NULL, '".$target_dir."');");
                    $res = ft_get_query("SELECT * FROM  `photos` WHERE address LIKE  '".$target_dir."'");
                    $ph = $res['id'];
                    ft_execute_query("INSERT INTO `ctrler_effects`(`name`,`id_photo`) VALUES ('".$_POST['effect_name']."',".$ph.");");
                    echo "Succesfully added! Redirecting...";
                    header("Location: /admin.php");
                }
            }
        }
    }
    else
    {
    
    
    
    
    ?>
    <form action="/add_effect.php" method="POST" enctype="multipart/form-data">
        <div id="form">
            <table>
                <tr>
                    <td class="right"><label for="effect_name">Name</label></td>
                    <td><input type="text" id="effect_name" name="effect_name" required/></td>
                </tr>
                <tr>
                    <td class="right"><label for="effect_image">Image</label></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="file" id="effect_image" accept="image/*" multiple name="image" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" name="submit" value="Submit"/></td>
                </tr>
            </table>
        </div>
    </form>
    <?php
    }
}
else
{
    echo "<script type='text/javascript'>alert(\"You Have no permision on this page\");window.location = '/';</script>";
}
require_once "footer.php";
?>