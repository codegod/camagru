<?php
require_once "config/functions.php";

$PageTitle = "Camagru | Admin Page";
if (ft_is_admin())
{
    require_once "header.php";
    require_once "menu_admin.php";
    $sql = "select photos.id, photos.address, users.name, users.surname, 
        count(distinct likes.id) as likes, count(distinct comments.id) as comments from ctrler_photo 
        inner join users on users.id=ctrler_photo.id_user 
        inner join photos on ctrler_photo.id_photo=photos.id 
        left join likes on likes.id_photo=photos.id 
        left join comments on comments.id_photo=photos.id group by photos.id order by id desc limit 6;";
        
    $photos = ft_get_all_queries($sql);
    
    ?>
    
    <div id="content">
        <center><h3>Recent Activity</h3></center>
        
        <?php if($photos)
        foreach ($photos as $photo) {
        ?>
        <div id="post">
            <div class="profilephoto">
                <img src="<?php echo $photo['address']; ?>"></img>
            </div>
            <div class="description">
                <span><?php echo $photo['name']." ".$photo['surname']; ?> added new photo</span>
                <span><?php echo $photo['likes']; ?> likes and <?php echo $photo['comments']; ?> comments</span>
            </div>
        </div><br>
        <?php
        }
        ?>
    </div>
    
    <?php
    require_once "footer.php";
}
else
{
    echo "<script type='text/javascript'>alert(\"You Have no permision on this page\");window.location = '/';</script>";
}
?>