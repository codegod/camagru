<?php
$PageTitle = "Camagru | Users";
require_once "header.php";
require_once "config/functions.php";
require_once "menu_admin.php";

if (ft_is_admin())
{
    $sql = "select users.id, users.name, users.surname, users.email, users.number, 
            users.login, users.verified, users.birth_date, users.signup_date, 
            photos.address from users 
            join photos on photos.id=users.id_photo; ";
    
    $users = ft_get_all_queries($sql);
?>
<style type="text/css">
    table tr{
        text-align:left;
    }
</style>
    <div id="content">
        <table>
            <tr>
                <th>photo</th>
                <th>username</th>
                <th>name</th>
                <th>surname</th>
                <th>number</th>
                <th>email</th>
                <th>verified</th>
                <th>birth date</th>
                <th>signup date</th>
            </tr>
            <?php foreach($users as $user){ ?>
             <tr>
                 <td><img src="<?php echo $user['address']; ?>" width='100' height='100'></img></td>
                <td><?php echo $user['login']; ?></td>
                <td><?php echo $user['name']; ?></td>
                <td><?php echo $user['surname']; ?></td>
                <td><?php echo $user['number']; ?></td>
                <td><?php echo $user['email']; ?></td>
                <td><?php echo $user['verified']==1 ? 'yes' : 'no'; ?></td>
                <td><?php echo $user['birth_date']; ?></td>
                <td><?php echo $user['signup_date']; ?></td>
            </tr>
            <?php } ?>
        </table>
    </div>
<?php
}
else
{
    echo "<script type='text/javascript'>alert(\"You Have no permision on this page\");window.location = '/';</script>";
}
require_once "footer.php";
?>