
<?php
$PageTitle = "Camagru | Public photos";

require_once "config/functions.php";
require_once "header.php";

if ($_GET['show'] == 'photo' && $_GET['id']) {
    

$sql = 'select photos.address as address, users.name as name, users.surname as surname, 
        count(distinct likes.id) as likes, count(distinct comments.id) as comments from photos 
        inner join ctrler_photo on ctrler_photo.id_photo=photos.id 
        inner join users on ctrler_photo.id_user=users.id 
        left join likes on likes.id_photo=photos.id 
        left join comments on comments.id_photo=photos.id 
        where photos.id = '.$_GET['id'].';';
        
        $res = ft_get_query($sql);
        
        $sql = 'select users.name as name, users.surname as surname, comments.content as content, add_date 
        from comments inner join users on users.id=comments.id_user where comments.id_photo='.$_GET['id'].';';
        ?>
        <div id="content">

            <div class="photo">
                <img src="<?php echo $res['address']; ?>"  />
                <address>
                    <span><a id='likebtn' href="#">Likes:</a></span><span id='likesec'> <?php echo $res['likes']; ?></span>
                    <span>Comments: <?php echo $res['comments']; ?></span>
                </address></a>
            </div>
            <div class="comments">
                <div id="text">
                    
                    <?php
                    $res = ft_get_all_queries($sql);
                    if ($res)
                        foreach ($res as $comment) {
                    ?>
                    <div class="comment-wrapper">
                        <span style="font-weight:bold;"><?php echo $comment['name'].' '.$comment['surname']; ?></span><br>
                        
                        <span style="font-style:italic;"><?php echo $comment['content']; ?></span><br>
                        <span id="date-for-comment">Added: <?php echo $comment['add_date']; ?></span><br>   
                    </div>
                    <?php } ?>
                </div>
            <form id="formcomment">
                <textarea cols=50 rows=5 name="content" placeholder='To leave a comment you need to be authenticated.' disabled id='con'></textarea><br>
                <input type="submit" id='commbutton' value="Go to login" />
            </form>
            </div>
        </div>
        <script type="text/javascript">
            document.getElementById("commbutton").addEventListener("click", function(e){
                e.preventDefault();
                document.location = "/login.php";
            });
            document.getElementById("likebtn").addEventListener("click", function(e){
                e.preventDefault();
                document.location = "/login.php";
            });
        </script>
        <?php
}
?>