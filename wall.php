<?php
$PageTitle = "Camagru | Wall";

session_start();

if ((($_GET['show'] == 'photo' && $_GET['id']) && (!isset($_SESSION['logged_in']) && !isset($_SESSION['user_id']))))
{
    header('Location: public.php?show=photo&id='.$_GET['id']);
    exit();
}

require_once "header.php";
require_once "menu.php";

require_once "config/functions.php";


?>

<?php

if ($_GET['show'] == 'users') {
    $sql = "select users.id, users.name, users.surname, photos.address from ctrler_photo 
            inner join users on ctrler_photo.id_user=users.id 
            inner join photos on photos.id=users.id_photo group by users.id;";
    
    $users = ft_get_all_queries($sql);
    ?>
    <div id="content">
        <p>Users that added photos on Camagru:</p>
    <?php
    if ($users)
        foreach ($users as $user) {
    ?>
            <div class="product">
                <a href="/wall.php?show=userphotos&id=<?php echo $user['id']; ?>">
                    <img  src="<?php echo $user['address']; ?>" />
                    <address>
                        <span><?php echo $user['name']." ".$user['surname']; ?></span>
                    </address>
                </a>
            </div>
    <?php
    } else {
        ?>
        
        <?php
    }
    ?>
    </div>
    <?php
}
elseif ($_GET['show'] == 'userphotos' && $_GET['id'])
{
        $sql = "select photos.id, photos.address, users.name, users.surname, 
        count(distinct likes.id) as likes, count(distinct comments.id) as comments from ctrler_photo 
        inner join users on users.id=ctrler_photo.id_user 
        inner join photos on ctrler_photo.id_photo=photos.id 
        left join likes on likes.id_photo=photos.id 
        left join comments on comments.id_photo=photos.id where ctrler_photo.id_user=".$_GET['id']." group by photos.id;";
        
        $photos = ft_get_all_queries($sql);
        // var_dump($photos);
        ?>
        <div id="content">
            <p>Photos added by <?php echo $photos[0]['name']." ".$photos[0]['surname']; ?></p>
        <?php
        if ($photos)
            foreach ($photos as $photo) {
            ?>
                   
                    
                <div class="product">
                    <a href="/wall.php?show=photo&id=<?php echo $photo['id']; ?>">
                        <img src="<?php echo $photo['address']; ?>" />
                        
                    </a>
                    <address>
                            <span>Likes: <?php echo $photo['likes']; ?> Comments: <?php echo $photo['comments']; ?></span>
                        </address>
                </div>
        <?php }
        else {
        ?>      
          <center><h3>No photos added by this user :((</h3></center>
        <?php } ?>
         </div>
    
        <?php
}
elseif ($_GET['show'] == 'photo' && $_GET['id'])
{
        $sql = 'select photos.address as address, users.name as name, users.surname as surname, 
        count(distinct likes.id) as likes, count(distinct comments.id) as comments from photos 
        inner join ctrler_photo on ctrler_photo.id_photo=photos.id 
        inner join users on ctrler_photo.id_user=users.id 
        left join likes on likes.id_photo=photos.id 
        left join comments on comments.id_photo=photos.id 
        where photos.id = '.$_GET['id'].';';
        
        $res = ft_get_query($sql);
        
        $sql = 'select users.name as name, users.surname as surname, comments.content as content, add_date 
        from comments inner join users on users.id=comments.id_user where comments.id_photo='.$_GET['id'].';';
        
        ?>
        <div id="content">

            <div class="photo">
                <img src="<?php echo $res['address']; ?>"  />
                <address>
                    <span><a id='likebtn' href="#">Likes:</a></span><span id='likesec'> <?php echo $res['likes']; ?></span>
                    <span>Comments: <?php echo $res['comments']; ?></span>
                </address></a>
            </div>
            <div class="comments">
                <div id="text">
                    
                    <?php
                    $res = ft_get_all_queries($sql);
                    if ($res)
                        foreach ($res as $comment) {
                    ?>
                    <div class="comment-wrapper">
                        <span style="font-weight:bold;"><?php echo $comment['name'].' '.$comment['surname']; ?></span><br>
                        
                        <span style="font-style:italic;"><?php echo $comment['content']; ?></span><br>
                        <span id="date-for-comment">Added: <?php echo $comment['add_date']; ?></span><br>   
                    </div>
                    <?php } ?>
                </div>
            <form id="formcomment">
                <textarea cols=50 rows=5 name="content" placeholder='Comment here' required id='con'></textarea><br>
                <input type="submit" id='commbutton' value="Place comment"/>
            </form>
            </div>
        </div>
        <script type="text/javascript">
        var commbutton = document.getElementById('commbutton');
        var subbutton = document.getElementById('likebtn');
        
        commbutton.addEventListener('click', function(e) {
            e.preventDefault();
            
            var con = document.getElementById('con').value;
            
            //XHRHttpRequest---------
            if (con != "") {
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '/controller.php');
                xhr.onreadystatechange = function(){
                    if(xhr.readyState != 4 || xhr.status != 200)
                        return;
                    document.getElementById('text').innerHTML += xhr.responseText;
                    document.getElementById('con').value = '';
                }
                
                xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                var res = 'scope=comment&id_photo=<?php echo $_GET['id']; ?>&content='+con;
                xhr.send(res);
            }
              
            //end
        });
        
        subbutton.addEventListener('click', function(e) {
            e.preventDefault();
            //XHRHttpRequest---------
        
            var xhr = new XMLHttpRequest();
            xhr.open('POST', '/controller.php');
            xhr.onreadystatechange = function(){
                if(xhr.readyState != 4 || xhr.status != 200)
                    return;
                
                if (xhr.responseText == 'like') {
                    var nr = document.getElementById('likesec');
                    var l = parseInt(nr.textContent);
                    l++;
                    nr.textContent = ' ' + l;
                }else if (xhr.responseText == 'unlike') {
                    var nr = document.getElementById('likesec');
                    var l = parseInt(nr.textContent);
                    l--;
                    nr.textContent = ' ' + l;
                }
            }
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            var res = 'scope=like&id_photo=<?php echo $_GET['id']; ?>';
            xhr.send(res);
              
            //end
        });
        </script>
        <?php
    }

else
    {
        $sql = "select photos.id, photos.address, users.name, users.surname, 
        count(distinct likes.id) as likes, count(distinct comments.id) as comments from ctrler_photo 
        inner join users on users.id=ctrler_photo.id_user 
        inner join photos on ctrler_photo.id_photo=photos.id 
        left join likes on likes.id_photo=photos.id 
        left join comments on comments.id_photo=photos.id group by photos.id;";
        
        $photos = ft_get_all_queries($sql);

        ?>
        <div id="content">
<?php
if ($photos)
    foreach ($photos as $photo) {
    ?>
                   
                    
            <div class="product">
                <a href="/wall.php?show=photo&id=<?php echo $photo['id']; ?>">
                    <img src="<?php echo $photo['address']; ?>" />
                    <address>
                        <span><?php echo $photo['name']." ".$photo['surname']; ?></span>
                    </address>
                </a>
            </div>
    <?php }
    else {
    ?>      
      <center><h3>No photos added by users :((</h3></center>
    <?php } ?>
     </div>

        <?php
    }

require_once "footer.php";
?>