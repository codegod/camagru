<?php
require_once "config/functions.php";

if (ft_is_admin())
{
  ft_not_expired_session();
  $user = ft_is_logged_in();
?>
   <nav id="primary_nav_wrap">
              <ul>
                  <li><a style="font-family: 'Clicker Script', cursive; font-size:42px; float:left; clear:both;" href="/">Camagru Admin</a></li>
              </ul>
              <ul style="float:right;">
                  <li><a href="#" onclick="event.preventDefault()"><?php echo $user['name']. " ".$user['surname']; ?></a>
                  <ul>
                    </li>
                    <?php if (ft_is_admin())
                    {
                    ?>
                    <li><a href="/admin.php">Admin Panel</a></li>
                    <?php
                    }
                    ?>
                    <li><a href="/logout.php">Logout</a></li>
                  </ul>
                  </li>
                 
              </ul><br><br>
  <ul>
    <li><a href="/">Home</a></li>
    <li><a href="add_effect.php">Add Effect</a></li>
    
    <li><a href="users.php">Users</a></li>
    
  </ul>
  </nav>

<?php
}
else
{
   echo "<script type='text/javascript'>alert(\"You Have no permision on this page\");window.location = '/';</script>";
}

?>