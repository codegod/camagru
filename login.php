<?php

$PageTitle = "Camagru | Login";

require_once "header.php";
require_once "config/functions.php";


session_start();

if ($_POST['login'] && $_POST['password'])
{
    $pass = hash("whirlpool", $_POST['password']);
    $res = ft_get_query("SELECT * FROM `users` WHERE login like '".$_POST['login']."';");
    if ($res)
    {
        if ($pass != $res['password'])
        {
            $err = "Wrong password!";
            $fpw = 1;
        }
        else if($res['verified'] == 0)
        {
            $err = 'Your account needs to be verified!<br> Check your email.';
        }
        else
        {
            $_SESSION['logged_in'] = "yes";
            $_SESSION['user_id'] = $res['id'];
            $_SESSION['start'] = time();
            $_SESSION['end'] = isset($_POST['remember']) ? time() + 31104000 : time() + 3600;
        }
    }
    else
        $err = "Wrong login!";
}

if (!isset($_SESSION['logged_in']) && !isset($_SESSION['user_id']))
{
    ?>
    <h1 class="logologin">
        <a href="/">Camagru</a>
    </h1>
    <form action="/login.php" method="POST">
        <div id="form">
                    <center><h3>Authentication</h3></center>
                
                    <label for="login">Login</label>
                    <input type="text" id="login" name="login" value="<?php echo $_POST['login'] ? $_POST['login'] : "";?>"  required/>
                    <label for="password">Password</label>
                    <input type="password" id="password" name="password" required/>
         
                    <?php
                    if (isset($err))
                    {
                    ?>
                    <p style="color:red;"><?php echo $err; ?></p>
                    
                    <?php
                    }
                    ?>
                    <br/>
                    <label for="remember">Remember-me</label>
                    <input id="remember" type="checkbox" name='remember' value="2"/><br>
                    <?php if (isset($fpw)) { ?>
                    <a href="/reset.php" style="text-decoration:none;cursor:pointer;">Forgot password?</a><br>
                    <?php } ?>
                    <a href="/register.php" style="text-decoration:none;cursor:pointer;">Don't have an account?</a>
                
                    <input class="subbutton" type="submit" value="Login"/>
                
        </div>
    </form>
    <?php
}
else
{
    header("Location: /");
}

?>