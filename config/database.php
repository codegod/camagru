<?php
    
    function get_connection()
    {
        $DB_DSN = 'mysql:host=localhost;dbname=db_camagru';
        $DB_USER = 'constantinc';
        $DB_PASSWORD = '';
        try {
            $dbh = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            return(false);
        }
        return ($dbh);
    }
    
    function ft_execute_query($sql)
    {
        try {
            $dbh = get_connection();
            $stmt   = $dbh->prepare($sql);
            $result = $stmt->execute();
            $dbh = null;
            if ($result)
                return (true);
            else
                return (false);
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            return(false);
        }
    }
    
    function ft_get_query($sql)
    {
        try {
            
            $dbh = get_connection();
            $stmt   = $dbh->prepare($sql);
            $result = $stmt->execute();
            $data   = $stmt->fetch(PDO::FETCH_ASSOC);
            $dbh    = null;
            return ($data);
            
        } catch (PDOException $e) {
        
            print "Error!: " . $e->getMessage() . "<br/>";
            return(false);
        
        }
    }
    
    function ft_get_all_queries($query)
    {
       try {
            $dbh = get_connection();
            $stmt   = $dbh->prepare($query);
            $result = $stmt->execute();
            $i = 0;
            while ($res  = $stmt->fetch(PDO::FETCH_ASSOC))
                $data[$i++] = $res;
            $dbh = null;
            return ($data);
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            return(false);
        }
    }
?>