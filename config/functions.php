<?php

require_once "database.php";

function ft_copy_to_center($dst, $src, $result) {
    list($dst_w, $dst_h) = getimagesize($dst);
    list($src_w, $src_h) = getimagesize($src);
    
    $image = imagecreatefrompng($dst);
    $frame = imagecreatefrompng($src);
    
    $posx = $dst_w / 2 - $dst_w / 6;
    $posy = $dst_h / 2 - $dst_h / 6;
    
    imagecopyresized($image, $frame, $posx, $posy, 0, 0, $dst_w / 3, $dst_h / 3, $src_w, $src_h);
    
    imagepng($image, $result);
}

function ft_base64_to_png($b64, $result) {
    $b64 = str_replace('data:image/png;base64,', '', $b64);
    $b64 = str_replace(' ', '+', $b64);
    $b64 = base64_decode($b64);
    file_put_contents($result, $b64);
}

function ft_import_tables($file)
{
    $templine = '';
    
    $lines = file($file);

    foreach ($lines as $line)
    {

        if (substr($line, 0, 2) == '--' || $line == '')
            continue;
        
        $templine .= $line;
    
        if (substr(trim($line), -1, 1) == ';')
        {
            ft_execute_query($templine);
            $templine = '';
        }
    }
    return (true);
}

function ft_is_logged_in()
{
    session_start();
    if (isset($_SESSION['logged_in']) && isset($_SESSION['user_id']))
        $user = ft_get_query("SELECT * FROM  `users` WHERE id =".$_SESSION['user_id']."");
    return ($user);
}

function ft_not_expired_session()
{
    session_start();
    if (isset($_SESSION['end']) && $_SESSION['end']  < time())
        header("Location: /logout.php");
}

function ft_is_admin()
{
    $user = ft_is_logged_in();
    if ($user)
    {
        $res = ft_get_query("SELECT * FROM `ctrler_priv` WHERE `id_user` = " . $user['id'] . " AND `id_priv` = 1;");
        if (count($res) > 1)
            return (true);
    }
    return (false);
}

function ft_is_set_admin()
{
    $res = ft_get_query("select email from users inner join ctrler_priv on id_user = users.id and id_priv = 1;");
    if ($res != false)
        return (true);
    return (false);
}

function ft_liked($user_id, $photo_id) {
    $sql = "select likes.id as id from photos inner join likes on likes.id_photo=photos.id where likes.id_user=$user_id and photos.id=$photo_id;";
    $res = ft_get_query($sql);
    if ($res)
        return (true);
    return (false);
}

function ft_like($user_id, $photo_id) {
    ft_execute_query("insert into likes (id_user, id_photo) values ($user_id, $photo_id);");
}

function ft_unlike($user_id, $photo_id) {
    ft_execute_query("delete from likes where id_user=$user_id and id_photo=$photo_id;");
}

function ft_mail($to, $subject, $message, $headers) {
    file_put_contents("mails.txt", "To: " . $to . "\nSubject: " . $subject . "\nMessage: " . $message . "\nHeaders: " . $headers);
}
        
function send_mail_confirm($userdata) {
    $to = $userdata['email'];
    $subject = "Account confirm | Camagru";
    $link = "http://" . $_SERVER['HTTP_HOST'] . "/confirm.php?token=" . hash("ripemd160", $userdata['password'].$userdata['email']) . "&uid=" . $userdata['id'];
    $message = '<h3>Hello '.$userdata['name']." ".$userdata['surname'].', to confirm you account on Camagru access this link: <a href="'.$link.'">Confirm</a></h3>';
    $headers = 'From: noreply@camagru.com' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
    ft_mail($to, $subject, $message, $headers);
    //mail($to, $subject, $message, $headers);
}

function send_mail_reset($email) {
    $to = $email;
    $subject = "Reset password | Camagru";
    $userdata = ft_get_query("select * from users where email like '".$email."'");
    $link = "http://" . $_SERVER['HTTP_HOST'] . "/reset.php?token=" . hash("ripemd160", $userdata['password'].$userdata['email']) . "&uid=" . $userdata['id'];
    $message = '<h3>Hello '.$userdata['name']." ".$userdata['surname'].', to reset password for your account on Camagru access this link: <a href="'.$link.'">Reset</a></h3>';
    $headers = 'From: noreply@camagru.com' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
    ft_mail($to, $subject, $message, $headers);
    //mail($to, $subject, $message, $headers);
}

function ft_add_comment($user_id, $photo_id, $content) {
    ft_execute_query("insert into comments (id_user, id_photo, content) values ($user_id, $photo_id, '$content');");
    $user = ft_get_query("select name, surname from users where id=$user_id;");
    return ("<span>Author: ".$user['name'].' '.$user['surname']."</span><br><span>Comment: ".$content."</span><br><br>");
}

?>